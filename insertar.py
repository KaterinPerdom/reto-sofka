import pymysql

class DataBase:
    def __init__(self):
        self.connection = pymysql.connect(
            host='localhost',
            user= 'kate', 
            passwd='tandesordenado', 
            db='Transporte' 
        )
        self.cursor =self.connection.cursor()

        print("Conexión establecida")

    def ingresarMunicipio(self, id, nombre, departamento, municipio_cerca_ref_id):

        sql = 'INSERT INTO Municipio (id, nombre, departamento, municipio_cerca_ref_id) VALUES (%s,%s,%s,%s)'
        
        tupla = (id, nombre, departamento, municipio_cerca_ref_id)
        
        try:
            self.cursor.execute(sql,tupla)
            self.connection.commit()

        except Exception as e:
            raise

    def consultarMunicipio(self, id):

        sql = 'SELECT id, nombre, departamento, municipio_cerca_ref_id FROM Municipio WHERE departamento={}'.format(id)
        
        try:
            self.cursor.execute(sql)
            municipio = self.cursor.fetchone()

            print("id :",municipio[0])
            print("nombre :",municipio[1])
            print("departamento :", municipio[2])
            print("municipio_cerca_ref_id :", municipio[3])
        
        except Exception as e:

            raise

    def consultarMunicipioPorDepartamento(self,departamento):

        sql ='SELECT * FROM Municipio WHERE departamento=%s'

        try:
            self.cursor.execute(sql,departamento)
            municipios = self.cursor.fetchall()
            
            for municipio in municipios:
                print("Id = ", municipio[0])
                print("Nombre = ", municipio[1])
                print("municipio_cerca_ref_id = " , municipio[3], "\n")
        
        except Exception as e:
            raise

    def ingresarMunicipioCercano(self, ref_id, municipio_id):

        sql = 'INSERT INTO Municipio_Cerca (ref_id, municipio_id) VALUES (%s,%s)'
        
        tupla = (ref_id, municipio_id)
        
        try:
            self.cursor.execute(sql,tupla)
            self.connection.commit()

        except Exception as e:
            raise
            

database = DataBase()
#database.ingresarMunicipio(5621, 'San Agustin', 'Huila', 56785)
#database.consultarMunicipioPorDepartamento('Huila')
database.ingresarMunicipioCercano(5621,5230)


  

